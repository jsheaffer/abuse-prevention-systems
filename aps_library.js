/**
 * Abuse Prevention Systems Library functions
 * @author JS
 */

define(['N/search', 'N/record', 'N/format'],

function( search, record, format ) {

    function getLastTest( data ) {

        if ( data.length > 1 ) {

            for ( var i = 0; i < data.length; i ++ ) {

                var test = data[i];

                if ( test.complete_date ) {

                    return test;
                }
            }
        }
        else {

            return data.shift();
        }
    }

    function formatDate( data ) {

        if ( data ) {

            var date = data.split("T")[0].split("-");

            return format.parse({
                value: [ date[1], date[2], date[0] ].join("/"),
                type: format.Type.DATE
            });
        }
        else {

            return '';
        }
    }

    return {
        getLastTest : getLastTest,
        formatDate  : formatDate
    };
});

/**
 * Abuse Prevention Systems - Retests
 * @author JS
 *
 * _@NApiVersion 2.x
 * _@NScriptType MapReduceScript
 */

define(['N/search', 'N/runtime', './aps_library', './aps_settings'], function( search, runtime, library, settings ) {

    // Functions
    function getInputData() {

        var script   = runtime.getCurrentScript();
        var list     = script.getParameter({ name: 'custscript_aps_search' });

        log.audit({
            title: 'Abuse Prevention Systems - Re-test',
            details: 'Search: ' + list
        });

        return search.load({
            id : list
        });
    }

    function reduce( context ) {

        var script = runtime.getCurrentScript();

        log.debug({
            title: 'Reduce Function Usage - Start',
            details: script.getRemainingUsage()
        });

        log.debug({
            title: 'Reduce Context',
            details: context
        });

        var result = JSON.parse( context.values[0] );

        log.debug({
            title: 'Employee',
            details: result
        });

        var response = settings.sendRequest({
            method : 'POST',
            target : '/api/v1/users/{id}/assign_training'.replace( '{id}', result.values.custrecord_employee_id ),
            record : {}
        });

        /**
        * @TODO Any additional action?
        */

        log.debug({
            title: 'Reduce Function Usage - End',
            details: script.getRemainingUsage()
        });
    }


    function summarize( summary ) {

        log.audit({
			title: 'Summary',
			details: summary
		});
    }

    // Actions
	return {
		getInputData : getInputData,
        reduce       : reduce,
		summarize    : summarize
	};

});

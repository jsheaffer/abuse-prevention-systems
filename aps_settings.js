/**
 * Abuse Prevention Systems Settings
 * @author JS
 */

define(['N/encode', 'N/https', 'N/runtime'], function( encode, https, runtime ) {

//  Functions
    function getEnvironment() {

        switch ( runtime.envType ) {
            case 'PRODUCTION':

                return {
                    url   : "https://safetysystem.abusepreventionsystems.com",
                    token : '83143bebe399ddc797d433bc85a649ba'
                };
                break;
            case 'SANDBOX':

                return {
                    url   : "https://staging.abusepreventionsystems.com",
                    token : '4js9fs951jamb042jd8fj5s'
                };
                break;
        }
    }

    function getSettings( request ) {

        var environment = getEnvironment();

        return {
            url     : environment.url.concat( request.target ),
            headers : {
                "Content-Type"  : "application/json",
        	    "Authorization"	: "Token token=".concat( environment.token )
            }
        };
    }

    function sendRequest( request ) {

        var settings = getSettings( request );

        try {

            var response = https.request({
                method  : request.method,
    			url     : settings.url,
    			body    : JSON.stringify( request.record ),
    			headers : settings.headers
            });

            log.debug({
                title: 'APS | Response',
                details: response
            });

            return response;
        }
        catch ( error ) {

            log.error({
                title: 'APS | sendRequest error',
                details: error
            });

            return false;
        }
    }

//  Actions
    return {
        sendRequest : sendRequest
    };
});

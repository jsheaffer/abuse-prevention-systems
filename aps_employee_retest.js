/**
 * Mass Update - load and submit record results
 * @author JS
 *
 * _@NApiVersion 2.x
 * _@NScriptType MassUpdateScript
 */

define(['N/record', 'N/search', './aps_settings'],

function( record, search, settings ) {

    return {
        each: function( params ) {

            var result = search.lookupFields({
                type : params.type,
                id   : params.id,
                columns : [
                    'custrecord_employee_id',
                    'custrecord_employee_first_name',
                    'custrecord_employee_last_name'
                ]
            });

            var entity = [
                params.id,
                result.custrecord_employee_first_name,
                result.custrecord_employee_last_name
            ].join(' ');

            log.debug({
                title: 'APS | Assign Training',
                details: entity
            });

            var response = settings.sendRequest({
                method : 'POST',
                target : '/api/v1/users/{id}/assign_training'.replace( '{id}', result.custrecord_employee_id ),
                record : {}
            });

            if ( response && response.code === 201 ) {

                log.audit({
                    title: 'APS | Training Assigned',
                    details: entity
                });
            }
            else {

                log.error({
                    title: 'APS | Training NOT Assigned',
                    details: entity
                });
            }
        }
    };
});

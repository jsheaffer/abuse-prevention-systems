/**
 *	JavaScript to update Dayforce Employee APS scores
 *
 *  Nobel Learning Communities Inc.
 * 	@author JS
 *  @version 1.0
 */


// var SENDURL = "https://safetysystem.abusepreventionsystems.com/api/v1/users/";
var HEADERS = {
	"Content-Type"  : "application/json",
	"Authorization"	: "Token token=83143bebe399ddc797d433bc85a649ba",
};
var METHOD  = "GET";

function update_scores( type, id ) {

	var	employee = nlapiLoadRecord( type, id );

    var aps_id = employee.nlapiGetFieldValue("custrecord_employee_aps_id");
	// SENDURL = SENDURL + employee.getFieldValue( 'custrecord_employee_id' );
    var SENDURL = "https://safetysystem.abusepreventionsystems.com/api/v2/users/{id}/trainings";
        SENDURL = SENDURL.replace( "{id}", aps_id );

	try {

		var request = nlapiRequestURL( SENDURL, null, HEADERS, null, METHOD );
	}
	catch ( error ) {

		nlapiLogExecution( 'error', 'Code',    error.getCode() );
		nlapiLogExecution( 'error', 'Details', error.getDetails() );

		return false;
    }

    var user = JSON.parse( request.getBody() );
        user = user.shift();

	// if ( ! user.employee_id )
	// 	return false;

	if ( user.complete_date ) {

		var date  = new Date( user.complete_date );
		var	day   = date.getDate();
		var	month = date.getMonth() + 1;
		var	year  = date.getFullYear();

		var renewal = month.toString() + '/' + day.toString() + '/' + year.toString();
	}
	else {

		var renewal = '';
	}

	employee.setFieldValue( 'custrecord_employee_score', user.score );
	employee.setFieldValue( 'custrecord_employee_complete_date', renewal );

	nlapiSubmitRecord( employee );

}

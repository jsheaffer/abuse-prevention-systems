/**
 * Abuse Prevention Systems - Employee Update
 * @author JS
 *
 * _@NApiVersion 2.x
 * _@NScriptType WorkflowActionScript
 */


define(['N/search', './aps_library', './aps_settings'],

function( search, library, settings ) {

    function deleteEmployee( context ) {

        var employee = context.newRecord;

        var response = settings.sendRequest({
            method : 'DELETE',
            target : '/api/v2/users/{id}'.replace( '{id}', employee.getValue({ fieldId: 'custrecord_employee_aps_id' }) ),
            record : {}
        });

    
    }

    return {
        onAction: deleteEmployee
    };
});

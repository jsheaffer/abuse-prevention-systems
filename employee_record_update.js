/**
 *	JavaScript to update Teacher Records - MinistrySafe
 *
 *  Nobel Learning Communities Inc.
 * 	@author JS
 *  @version 1.0
 */


var sendurl = "https://staging.abusepreventionsystems.com/api/v1/users/";
var headers = {
	"Content-Type"  : "application/json",
	"Authorization"	: "Token token=4js9fs951jamb042jd8fj5s",
};
var method = "POST";

/**
 * Update dayforce employees in Abuse Prevention Systems
 * @param {Object} id
 */
function update_employee( id ) {

	var employee = {};

	//	Delete/Inactivate
	if ( nlapiGetFieldValue('isinactive') == 'T' ) {

		nlapiLogExecution( 'system', 'Day Force Employees',  'Record is inactive, delete record' );

		sendurl = sendurl + nlapiGetFieldValue('custrecord_employee_id');
		method  = "DELETE";

		nlapiLogExecution( 'system', 'Day Force Employees URL',  sendurl );
	}
	//	Create/Update
	else {

		var employee = {
			user : {
				first_name  : nlapiGetFieldValue('custrecord_employee_first_name'),
				last_name   : nlapiGetFieldValue('custrecord_employee_last_name'),
				email	    : nlapiGetFieldValue('custrecord_employee_email'),
				external_id : nlapiGetFieldValue('custrecord_employee_id'),
			},
			tag_list : nlapiGetFieldValue('custrecord_employee_location'),
		};

		nlapiLogExecution( 'system', 'Day Force Employees record',  JSON.stringify( employee ) );
	}

	employee = JSON.stringify( employee );

	try {

		var entityTitle = nlapiGetFieldValue('custrecord_employee_id') + ' ' + nlapiGetFieldValue('custrecord_employee_first_name') + ' ' + nlapiGetFieldValue('custrecord_employee_last_name');

		var response = nlapiRequestURL( sendurl, employee, headers, method );
		nlapiLogExecution( 'system', 'Day Force Employees',  entityTitle + ' | Sent Successfully.' );
		nlapiLogExecution( 'system', 'Day Force Employees Response', response.getBody() );
		nlapiLogExecution( 'system', 'Day Force Employees Headers', JSON.stringify( response.getAllHeaders() ) );
	}
	catch ( err ) {

		nlapiLogExecution( 'error', 'Code',    err.getCode() );
		nlapiLogExecution( 'error', 'Details', err.getDetails() );
    }
}

/**
 * Update record via Mass Update
 * @param {Object} type, id
 */
function mass_update_employee( type, id ) {

	var record = nlapiLoadRecord( type, id );
	nlapiSubmitRecord( record );
}

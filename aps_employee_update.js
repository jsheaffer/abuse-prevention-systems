/**
 * Abuse Prevention Systems - Get Employee score
 * @author JS
 *
 * _@NApiVersion 2.x
 * _@NScriptType WorkflowActionScript
 */


define(['N/record', './aps_library', './aps_settings' ],

function( record, library, settings ) {

    function updateEmployee( context ) {

        var employee = context.newRecord;

        var entitytitle = [
            employee.id,
            employee.getValue({ fieldId: 'custrecord_employee_first_name' }),
            employee.getValue({ fieldId: 'custrecord_employee_last_name' })
        ].join(' ');

        log.debug({
            title: 'APS | Day Force Employee',
            details: entitytitle
        });

        var response = settings.sendRequest({
            method : 'GET',
            target : '/api/v2/users/{id}/trainings'.replace( '{id}', employee.getValue({ fieldId: 'custrecord_employee_aps_id' }) ),
            record : {}
        });

        if ( response.code === 200 ) {

            var data = JSON.parse( response.body );
            var test = library.getLastTest( data );

            if ( test ) {

                log.debug({
                    title: 'APS | Latest Test',
                    details: test
                });

                var date = library.formatDate( test.complete_date );

                log.debug({
                    title: 'APS | Employee Test Result',
                    details: entitytitle + ' | Score: ' + test.score + ', Date: ' + date
                });

                employee.setValue({
                    fieldId : 'custrecord_employee_score',
                    value   : test.score
                });
                employee.setValue({
                    fieldId : 'custrecord_employee_complete_date',
                    value   : date
                });
            }
        }
        else {

            log.error({
                title: 'APS | Request Failed',
                details: response
            });
        }
    }

//	Actions
	return {
		onAction: updateEmployee
	};
});

/**
 * Abuse Prevention Systems - Employee Update
 * @author JS
 *
 * _@NApiVersion 2.x
 * _@NScriptType WorkflowActionScript
 */


define(['N/search', './aps_library', './aps_settings'],

function( search, library, settings ) {

    function createEmployee( context ) {

        var employee = context.newRecord;

        var entitytitle = [
            employee.id,
            employee.getValue({ fieldId: 'custrecord_employee_first_name' }),
            employee.getValue({ fieldId: 'custrecord_employee_last_name' })
        ].join(' ');

        log.debug({
            title: 'APS | Day Force Employee',
            details: entitytitle
        });

        var response = settings.sendRequest({
            method : 'POST',
            target : '/api/v2/users/',
            record : {
    			user : {
    				first_name  : employee.getValue({ fieldId: 'custrecord_employee_first_name' }),
    				last_name   : employee.getValue({ fieldId: 'custrecord_employee_last_name' }),
    				email	    : employee.getValue({ fieldId: 'custrecord_employee_email' }),
    				external_id : employee.getValue({ fieldId: 'custrecord_employee_id' }),
    			},
    			tag_list : employee.getValue({ fieldId: 'custrecord_employee_location' }),
    		}
        });

        if ( response && response.code == 201 ) {

            var data = JSON.parse( response.body );

            employee.setValue({
                fieldId: 'custrecord_employee_aps_id',
                value: data.id
            });
        }
    }

    return {
        onAction: createEmployee
    };
});

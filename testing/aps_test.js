require(['N/https']);
require(['N/format']);
var https = require('N/https');
var format = require('N/format');

var response = https.get({
    url: 'https://staging.abusepreventionsystems.com/api/v2/users/305960/trainings',
    headers : {
    	"Content-Type"  : "application/json",
    	"Authorization"	: "Token token=4js9fs951jamb042jd8fj5s",
    }
});
log.debug({
    title: 'response',
    details: response
});

var body = JSON.parse(response.body);
var data = body.shift();

var date = data.complete_date.split("T")[0].split("-");

var date = [ date[1], date[2], date[0] ].join("/");

log.debug({
    title: 'Formated Date',
    details: date
});

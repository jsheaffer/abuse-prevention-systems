/**
 *	JavaScript to update Dayforce Employee APS scores
 *
 *  Nobel Learning Communities Inc.
 * 	@author JS
 *  @version 1.0
 */


var SENDURL = "https://staging.abusepreventionsystems.com/api/v1/users/";
var HEADERS = {
	"Content-Type"  : "application/json",
	"Authorization"	: "Token token=4js9fs951jamb042jd8fj5s",
};
var METHOD = "GET";

function update_scores( type, id ) {
	
	var	employee = nlapiLoadRecord( type, id );

	SENDURL = SENDURL + employee.getFieldValue( 'custrecord_employee_id' );

	try {

		var request = nlapiRequestURL( SENDURL, null, HEADERS, null, METHOD );
	}
	catch ( error ) {

		nlapiLogExecution( 'error', 'Code',    error.getCode() );
		nlapiLogExecution( 'error', 'Details', error.getDetails() );

		return false;
    }

	var user = JSON.parse( request.getBody() );

	if ( ! user.employee_id )
		return false;

	if ( user.complete_date ) {

		var date  = new Date( user.complete_date );
		var	day   = date.getDate();
		var	month = date.getMonth() + 1;
		var	year  = date.getFullYear();

		var complete = month.toString() + '/' + day.toString() + '/' + year.toString();
	}
	else {

		var complete = '';
	}

	employee.setFieldValue( 'custrecord_employee_score', user.score );
	employee.setFieldValue( 'custrecord_employee_complete_date', complete );

	nlapiSubmitRecord( employee );

}
